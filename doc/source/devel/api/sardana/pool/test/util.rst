.. currentmodule:: sardana.pool.test.util

:mod:`~sardana.pool.test.util`
==============================

.. automodule:: sardana.pool.test.util

.. rubric:: Fixtures

.. hlist::
    :columns: 3

    * `pool`
    * `motctrl01`
    * `mot01`
    * `mot02`
    * `mot03`
    * `mot04`
    * `ctctrl01`
    * `ct01`
    * `ct02`
    * `ct03`
    * `ct04`
    * `zerodctrl01`
    * `zerod01`
    * `zerod02`
    * `zerod03`
    * `zerod04`
    * `onedctrl01`
    * `oned01`
    * `twodctrl01`
    * `twod01`
    * `iorctrl01`
    * `ior01`
    * `ior02`
    * `tgctrl01`
    * `tg01`
    * `slitctrl01`
    * `gap01`
    * `offset01`
    * `motgrp0102`


.. rubric:: "Factory as fixture"

.. hlist::
    :columns: 3

    * :func:`create_pool`
    * :func:`create_ctrl`
    * :func:`create_motor_ctrl`
    * :func:`create_counter_timer_ctrl`
    * :func:`create_zerod_ctrl`
    * :func:`create_oned_ctrl`
    * :func:`create_twod_ctrl`
    * :func:`create_ior_ctrl`
    * :func:`create_tg_ctrl`
    * :func:`create_element`
    * :func:`create_motor`
    * :func:`create_counter_timer`
    * :func:`create_zerod`
    * :func:`create_oned`
    * :func:`create_twod`
    * :func:`create_ior`
    * :func:`create_tg`

.. rubric:: Functions

.. hlist::
    :columns: 3

    * :func:`get_marker_attrs`
    * :func:`get_marker_attribute_values`
    * :func:`get_marker_kwargs`
    * :func:`set_obj_attrs`

.. rubric:: Classes

.. hlist::
    :columns: 3

    * :class:`ActionEvent`

.. autofunction:: set_obj_attrs
.. autofunction:: get_marker_attrs
.. autofunction:: get_marker_attribute_values
.. autofunction:: get_marker_kwargs
.. autofunction:: pool
.. autofunction:: motctrl01
.. autofunction:: mot01
.. autofunction:: mot02
.. autofunction:: mot03
.. autofunction:: mot04
.. autofunction:: ctctrl01
.. autofunction:: ct01
.. autofunction:: ct02
.. autofunction:: ct03
.. autofunction:: ct04
.. autofunction:: zerodctrl01
.. autofunction:: zerod01
.. autofunction:: zerod02
.. autofunction:: zerod03
.. autofunction:: zerod04
.. autofunction:: onedctrl01
.. autofunction:: oned01
.. autofunction:: twodctrl01
.. autofunction:: twod01
.. autofunction:: iorctrl01
.. autofunction:: ior01
.. autofunction:: ior02
.. autofunction:: tgctrl01
.. autofunction:: tg01
.. autofunction:: slitctrl01
.. autofunction:: gap01
.. autofunction:: offset01
.. autofunction:: motgrp0102
.. autofunction:: create_pool
.. autofunction:: create_ctrl
.. autofunction:: create_motor_ctrl
.. autofunction:: create_counter_timer_ctrl
.. autofunction:: create_zerod_ctrl
.. autofunction:: create_oned_ctrl
.. autofunction:: create_twod_ctrl
.. autofunction:: create_ior_ctrl
.. autofunction:: create_tg_ctrl
.. autofunction:: create_element
.. autofunction:: create_motor
.. autofunction:: create_counter_timer
.. autofunction:: create_zerod
.. autofunction:: create_oned
.. autofunction:: create_twod
.. autofunction:: create_ior
.. autofunction:: create_tg
.. autoclass:: ActionEvent
